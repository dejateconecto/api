//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');
var url = "https://api.mlab.com/api/1/databases/bdbanca3mb78949/collections/movimientos?apiKey=tg3ecrIo8YzTu_1ZHS5fsEzupTAaWmEB";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/movimientos',
  function(req, res){
    var clienteMlab = requestjson.createClient(url);
    clienteMlab.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          res.send(body);
        }
      });
  }
);

/**
*
1.- Crear el POST con cliente
2.- En polymer en en gestor-cliente en vez de trae los de Northwin, que consuma los de mlab
3.- Doquerizar front y back y comunicar los contenedores por los puertos 3001 y 3002
*/
app.post('/movimientos',
  function(req, res){
    var clienteMlab = requestjson.createClient(url);
    console.log(req.body);
    clienteMlab.post('', req.body,
      function(err, respM, requ){
        if(err){
          console.log(err);
        }else{
          res.send(requ);
        }
      });
  }
);

app.get('/',
  function(req, res){
    //res.send('My first \"Hola mundo\" from node.js');
    res.sendFile(path.join(__dirname, 'index.html'));
  }
);

app.post('/',
  function(req, res){
    res.send('Your POST method request has been accepted successfully.');
  }
);

app.put('/',
  function(req, res){
    res.send('Your PUT method request has been accepted successfully.');
  }
);

app.delete('/',
  function(req, res){
    res.send('Your DELETE method request has been accepted successfully.');
  }
);

var clientes = //"";
[{"id":"lu","name":"luis"},{"id":"ai","name":"aida"},{"id":"ma","name":"maria"}];

app.get('/clientes',
  function(req, res){
    //res.send('My first \"Hola mundo\" from node.js');
    res.send(clientes);
  }
);

app.get('/clientes/:idCliente',
  function(req, res){
    for (var i = 0; i < clientes.length; i++) {
      if (clientes[i].id == req.params.idCliente)
        res.send(clientes[i].name);
    }
    res.send("Client with " + req.params.idCliente + "has not been found CAMBIADA.");
  }
);

app.post('/clientes',
  function(req, res){
    res.send('Your POST method request for /clientes resource has been accepted successfully.');
  }
);

app.put('/clientes',
  function(req, res){
    res.send('Your PUT method request for /clientes resource has been accepted successfully.');
  }
);

app.delete('/clientes',
  function(req, res){
    res.send('Your DELETE method request for /clientes resource has been accepted successfully.');
  }
);
